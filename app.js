var config = require('config.js')
var http = require('./utils/httpHelper.js')
var util = require('./utils/util.js')
var WxParse = require('./wxParse/wxParse.js'); 
//app.js
App({
  globalData:{
    userInfo:null,
    title:"",
    shopInfo:{},
    pos:{}
  },
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var that = this;
    wx.getSystemInfo({//  获取页面的有关信息
      success: function (res) {
        wx.setStorageSync('systemInfo', res)
        var ww = res.windowWidth;
        var hh = res.windowHeight;
        that.globalData.ww = ww;
        that.globalData.hh = hh;
      }
    });
    http.request('/index/shop_setting', {}, function (res) {
      if (res.data.data.site_title) {
        wx.setNavigationBarTitle({
          title: res.data.data.site_title,
        })
      }
    }, 3600,'shopInfo');
	
  }
  
})