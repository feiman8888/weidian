# weidian

#### 介绍
Bidcms开源分销商城 自定义界面 分销版微信小程序

#### 软件架构
软件架构说明
Bidcms完整功能分销小程序，可自定义首页，会员中心，26套模板自由选择

#### 安装教程

1. 登录 http://shop.bidcms.com 上传商品,设置商城信息
2. 定制商城 请联系 qq:2559009123 微信：bidcms
3. 发布小程序

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)