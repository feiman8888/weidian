var config = require('../../config.js')
//index.js
//获取应用实例
var app = getApp()
var sta = require("../../utils/statistics.js");
var http = require('../../utils/httpHelper.js');
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderid:0,
    amount:0,
    status:-1,
    message: { title: '', content: '', url: '/pages/order/index', url_text: '确定'}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that=this;
    if(options && options.id){
      http.request(config.HTTP_API_URL + 'plugins/offlinepay/index.php?act=getOrderInfo', { id: options.id}, function (res) {
        that.setData({
          orderid: options.id,
          status:0,
          title: res.data.order_name,
          intro:res.data.order_intro
        });
      });
    } else {
      wx.showModal({
        title: '错误提示',
        content: '参数错误，无法调起支付',
        success: function (res) {
          return;
        }
      })
    }
  },
  changAmount: function (event){
    var amount = event ? event.detail.value : 0;
    this.setData({
      amount:amount
    });
  },
  pay:function(){
    let that=this;
    //获取订单信息
    var orderid=1;//this.data.orderid;
    http.request(config.HTTP_API_URL+'plugins/offlinepay/index.php?act=getPayInfo', { type: 'WXAPP', pay_type: 'order', id: orderid,amount:this.data.amount }, function (res) {
      var data = { type: 'WXAPP', notify_url: res.data.notify_url, info: res.data.info };
      http.request(config.HTTP_WXAPP_URL + "/pay/wxpay", data, function (params) {
        if (params.code == 1) {
          var json = params.data;
          json.success = function (res) {
            that.setData({
              status: 1,
              message: { title: '支付成功', content: '订单已经支付成功，点击订单列表', url: '/pages/order/index', url_text: '查看订单' }
            });
            that.updateOrderInfo(orderid, 1);
          }
          json.fail = function (res) {
            wx.showModal({
              title: '错误提示',
              content: '支付失败',
              success: function (res) {
                return;
              }
            })
          }
          wx.requestPayment(json);
        } else {
          wx.showModal({
            title: '错误提示',
            content: params.msg,
            success: function (res) {
              return;
            }
          })
        }
      });
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },
  goUrl: function (event) {
    var url = event ? event.currentTarget.dataset.url : '';
    util.goUrl(url, this);
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})