var config = require('../../config.js')
//index.js
//获取应用实例
var app = getApp()
var sta = require("../../utils/statistics.js");
var timer = require('../../utils/wxTimer.js');
var http = require('../../utils/httpHelper.js'); 
var util = require('../../utils/util.js'); 
var WxParse = require('../../wxParse/wxParse.js'); 

Page({
  data: {
    id:0,
    topScroll:[],
    footerNav:[],
    bannerIcon:[],
    pageIndex:1,
    pageSize:10,
    loading: true,
    loadtitle:"Bidcms正在努力为你加载更多...",
    hasMore:true,
    guessProducts:[],
    title:"首页",
    wxTimerList:{},
    wxParseData:{},
    searchKeyword:"",
    currentIndex:0,
    pages:{},
    extdata:{}
  }, 
  onShareAppMessage: function (res) {
    var title = this.data.title;
    return {
      title: title,
      path: 'pages/index/index?shopid=' + wx.getStorageSync('shopid')
    }
  },
  loadData:function(){
    let that=this;
    let url = "/index/index";
    http.request("/banner/getFooter", {}, function (res) {
      if (res.data.code == 0 && res.data.msg == 'success') {
        that.setData({ footerNav: res.data.data, footerType: res.data.style });
      }
    });
    http.request(url, this.data.extdata, function (res) {
      if (res.data.code == '0' && res.data.msg == 'success') {
        var len = res.data.data.LModules.length;
        for (var i = 0; i < len; i++) {
          var id = res.data.data.LModules[i].id;
          if (res.data.data.LModules[i].type == 20) {
            var endtime = res.data.data.LModules[i].content.act_end_time.replace('T', ' ');
            //开启第一个定时器
            var wxTimer = new timer({
              endTime: endtime,
              name: id,
              complete: function () {
                console.log("完成了")
              }
            })
            wxTimer.start(that);
          }
          if (res.data.data.LModules[i].type == 18 || res.data.data.LModules[i].type == 1) {
            var html = util.htmlDecode(res.data.data.LModules[i].content.fulltext);
            WxParse.wxParse(id, 'html', html, that, 0)
          }
          if(res.data.data.LModules[i].type==25){
            let index=i;
            http.request("/shop/getShops", that.data.extdata, function (cres) {
              res.data.data.LModules[index].content.dataset=cres.data.data;
              that.setData({ //因为是异步请求，需要重新为pages赋值
                pages: res.data.data
              });
            });
          }
        }
        var title = res.data.data.page.title;
        wx.setNavigationBarTitle({
          title: title,
        })
        that.setData({
          pages: res.data.data
        });
      }
    },3600,'index'+this.data.id);
  },
  //事件处理函数
  onLoad: function (options) {
    var that=this;
    var scene = decodeURIComponent(options.scene); //动态二维码
    if (scene && scene!='undefined') {
      wx.setStorageSync('sceneid', scene); //永久记录
    } else {
      wx.setStorageSync('shopid', options.shopid); //临时记录
    }
    this.setData({
      extdata: options
    });
	  let shopInfo = wx.getStorageSync('shopInfo');
    if (shopInfo){

      wx.setNavigationBarColor({
        frontColor: '#' + (shopInfo.data.nav_color && shopInfo.data.nav_color != '' ? shopInfo.data.nav_color : '000000').toLowerCase(),
        backgroundColor: '#' + (shopInfo.data.nav_bgcolor && shopInfo.data.nav_bgcolor != '' ? shopInfo.data.nav_bgcolor : 'ffffff').toLowerCase(),
      })
      wx.setNavigationBarTitle({
        title: !shopInfo.data.site_title ? '首页' : shopInfo.data.site_title,
      })
    }

    this.loadData();
   
  },
  goUrl:function(event){
    var url = event ? event.currentTarget.dataset.url : '';
    var rtype = event.currentTarget.dataset.type;
    if(url=='/index/index&wxapp=/pages/index/index'){
      return false;
    }
    util.goUrl(url, rtype);
  },
  openMap:function(event){
    let gps = event.currentTarget.dataset.gps.split(',');
    let name = event.currentTarget.dataset.name;
    let address = event.currentTarget.dataset.address;
    wx.openLocation({
      latitude: parseFloat(gps[0]),
      longitude: parseFloat(gps[1]),
      scale: 28,
      name: name,
      address: address
    })
  },
  goPhone:function(event){
	  var phone = event ? event.currentTarget.dataset.phone : '15503640042';
    util.goPhone(phone);
  },
  //输入框事件，每输入一个字符，就会触发一次  
  bindKeywordInput: function (e) {
    console.log("输入框事件")
    this.setData({
      searchKeyword: e.detail.value
    })
  },
  //点击搜索按钮，触发事件  
  keywordSearch: function (e) {
	var searchKeyword=this.data.searchKeyword;
    wx.navigateTo({
      url: "/pages/list/index?keyword=" + searchKeyword,
    });
  },
  onReady (options) {
    var that = this;
	
  },
  onShow () {
  },
  getTab(e){
	this.setData({currentIndex:e.currentTarget.dataset.id});
  }
})