//index.js
var config = require('../../config.js')
var http = require('../../utils/httpHelper.js')
var sta = require("../../utils/statistics.js");
var util = require("../../utils/util.js");
var WxParse = require('../../wxParse/wxParse.js');
//获取应用实例
var app = getApp();

Page({
  data: {
		indicatorDots: true,//是否显示面板指示点
		autoplay: true,  //是否自动切换
		interval: 5000, //自动切换时间
		duration: 1000,  //滑动时间
		buyNum:1,
		wxTimerList: {},
		wxParseData: {},
		selectedText:"颜色",
		index:0,
		goods_arr: [],
		goods_sku: [],
		price:0,
		original_price:0,
		comments:[],
		goods_id:0,
		page:0,
		loadMore:"查看更多...",
		income:0,
    networkType:'',
    income:0,
    carts:0
  },
  onLoad:function(options){
    var that = this;
    if(options.bid){
      wx.setStorageSync('bidcms_id', options.bid);
    }
    if (options.id>0){
      this.setData({
        goods_id: options.id
      });
	    this.getGoodsInfo();
      this.getComment();
    }
    wx.getNetworkType({
      success: function (res) {
        // 返回网络类型, 有效值：
        // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
        var networkType = res.networkType
        that.setData({
          networkType: networkType
        })
      }
    });
    var allGoods = wx.getStorageSync('allGoods');
    if (allGoods){
      this.setData({
        carts: allGoods.length
      })
    }
    
    let shopInfo = wx.getStorageSync('shopInfo');
    wx.setNavigationBarColor({
      frontColor: '#' + (shopInfo.nav_color && shopInfo.nav_color != '' ? shopInfo.nav_color : '000000').toLowerCase(),
      backgroundColor: '#' + (shopInfo.nav_bgcolor && shopInfo.nav_bgcolor != '' ? shopInfo.nav_bgcolor : 'ffffff').toLowerCase(),
    })
  },
	onShareAppMessage: function (res) {
		var that = this;
		var data=this.data.goods;
    var userInfo = wx.getStorageSync('userInfo');
		return {
		  title: data.title,
      path: '/pages/detail/index?id=' + data.id + '&bid=' + userInfo.uid,
		  imageUrl:data.pictures[0],
		  success: function(res) {
        // 转发成功
        console.log(res);
		  },
		  fail: function(res) {
			// 转发失败
		  }
		}
	},
  
    onShow:function (){
      sta();
	  
    },
    goUrl:function(event){
      var url = config.HTTP_API_URL+'mobile.php'+event.currentTarget.dataset.url;
      wx.navigateTo({
        url: '/pages/web/web?url=' + encodeURIComponent(url),
      })
    },
    showIncome:function(event){
	  var money = event.currentTarget.dataset.val;
      this.setData({
		  income:parseFloat(money/100).toFixed(2)
	  });
	},
	closeIncome:function(event){
      this.setData({
		  income:0
	  });
	},
	
    goPhone: function (event) {
      var phone = event.currentTarget.dataset.phone;
      util.goPhone(phone);
    },
    getComment:function(){
      var that = this;
      var page = this.data.page;
      var data = { id: this.data.goods_id,page:this.data.page }
      http.request("/item/getComment", data, function (res) {
        if (res.data.code == 0 && res.data.msg == 'success') {
          var len = res.data.data.length;
          var newDate = new Date();
          for (var i = 0; i < len;i++){
            newDate.setTime(res.data.data[i].updatetime * 1000);
            res.data.data[i].updatetime = newDate.getFullYear() + "-" + newDate.getMonth()+"-"+newDate.getDate()+" "+newDate.getHours()+":"+newDate.getMinutes();
            res.data.data[i].imglist = res.data.data[i].imglist!=""?res.data.data[i].imglist.split(','):[];
          }
          that.setData({
            comments:res.data.data,
            page: parseInt(page)+1,
            loadMore: len < 10?"没有更多内容了":"查看更多..."
          });
        }
      },3600,"goodsComment" + this.data.goods_id);
    },
    getGoodsInfo:function(){
        var that = this;
        var data = {id:this.data.goods_id}
        var shopInfo = wx.getStorageSync('shopInfo');
        wx.setNavigationBarColor({
          frontColor: '#' + (shopInfo.nav_color && shopInfo.nav_color != '' ? shopInfo.nav_color : '000000').toLowerCase(),
          backgroundColor: '#' + (shopInfo.nav_bgcolor && shopInfo.nav_bgcolor != '' ? shopInfo.nav_bgcolor : 'ffffff').toLowerCase(),
        })
        http.request("/goods/getGoodsInfo" ,data,function(res){
          if(res.data.code == 0){
            var goods = res.data.data;
            wx.setNavigationBarTitle({
              title: goods.title,
            })
            var skus=goods.skus;
            var sku_arr=that.data.goods_arr;
            for(var i in skus){
              sku_arr.push(skus[i].name+"("+skus[i].price+")");
            }
            var html = '<p>暂无内容</p>';
            WxParse.wxParse("content", 'html', html, that, 0);
            if (that.data.networkType == 'wifi') {
              that.getGoodsDetail();
            }
            that.setData({
              goods:goods,
              goods_sku:skus,
              goods_arr:sku_arr,
              price:goods.price,
              original_price:goods.original_price
            });
          }
        }, 3600, "goodsInfo" + this.data.goods_id);
      
    },
    getGoodsDetail:function(){
      var that = this;
      var data = { id: this.data.goods_id }
      http.request("/goods/getGoodsDetail", data, function (res) {
          if (res.data.code == 0) {
            var goods = res.data.data;
            if (goods.content) {
              var html = util.htmlDecode(goods.content.PModules[0].content.fulltext);
              WxParse.wxParse("content", 'html', html, that, 0)
            }
          }
        },3600,'goodsDetail'+that.data.goods_id);
    },
    buyCount:function(e){
        var id = e.currentTarget.id;
        var mgoods = this.data.goods;
        var maxCount =0;
        
        var count = this.data.buyNum;
        if(id == "add"){
            var maxCount = mgoods.quota > 0 ? mgoods.quota : 1;
            count = count + 1;
            if (mgoods.num < count) {
              wx.showToast({
                title: '库存不足',
              })
              return false;
            }
            if(count<=maxCount){
              this.setData({buyNum:count});
            }
        }else{
            count = (count>0)?count-1:0;
            if(count>0){
              this.setData({buyNum:count});
            }
        }
    },
    previewImg:function(e){
      var src = e.currentTarget.dataset.src;//获取data-src
      var imgList = e.currentTarget.dataset.list;//获取data-list
      //图片预览
      wx.previewImage({
        current: src, // 当前显示图片的http链接
        urls: imgList // 需要预览的图片http链接列表
      })
    },
  buyNow:function(e){
      var that=this;
      var id = e.currentTarget.id;
      var count = this.data.buyNum;
      count = count > 0 ? count : 1;
      var mgoods = this.data.goods;
      var token = wx.getStorageSync("token");
      if(!token){
        wx.redirectTo({
          url: '/pages/user/index',
        })
        return;
      }
      if(mgoods.num<count){
        wx.showToast({
          title: '库存不足',
        })
        return false;
      }
      var goods_sku=this.data.goods_sku;
      var sku_index=this.data.index;
      var goods = {
        id: mgoods.id,
        name: mgoods.title,
        img: mgoods.thumb,
        maxcount: mgoods.quota,
        maxnum: mgoods.num,
        shopid: mgoods.shop_id,
        price: that.data.price,
        buycount: count,
        sku: goods_sku[sku_index],
        point:mgoods.point
      };
      var allGoods = wx.getStorageSync('allGoods');
      if(allGoods){
        var add = 1;
        for(var i in allGoods){
          if(allGoods[i].id == goods.id){
            add = 0;
            allGoods[i].buycount += parseInt(goods.buycount);
          }
        }
        if (add == 1){
          allGoods.push(goods);
        }
       
      } else {
        var l = [];
        l.push(goods);
        allGoods = l;
      }
      wx.setStorageSync('allGoods',allGoods);
      that.setData({
        carts:allGoods.length
      });
      if (id == 'addCar') {
        wx.showToast({
          title: '加入购物车成功',
        })
      } else {
        wx.navigateTo({ url: '/pages/shoppingcar/index' });
      }
  },
  bindPickerChange: function(e) {
		var goods_sku=this.data.goods_sku;
		this.setData({
		  index: e.detail.value,
		  price:goods_sku[e.detail.value].price,
		  original_price:goods_sku[e.detail.value].oprice
		})
	},
})