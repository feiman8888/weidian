//index.js
//获取应用实例
var app = getApp()
var http = require('../../utils/httpHelper.js')
Page({
  data: {
    addressList:[]
  },

  selectTap: function (e) {
    wx.navigateBack({});
  },

  addAddess : function () {
    wx.navigateTo({
      url:"/pages/address-add/index"
    })
  },
  
  editAddess: function (e) {
    wx.navigateTo({
      url: "/pages/address-add/index?id=" + e.currentTarget.dataset.id
    })
  },
  
  onLoad: function () {
    console.log('onLoad')

   
  },
  onShow : function () {
    this.initShippingAddress();
  },
  initShippingAddress: function () {
    var that = this;
    http.request(
      '/user/shipping-address/list',
      {},
      function (res) {
        if (res.data.code == 0) {
          that.setData({
            addressList:res.data.data
          });
        }
      }
    )
  }

})
