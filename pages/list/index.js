var config = require('../../config.js')
var http = require('../../utils/httpHelper.js')
var util = require('../../utils/util.js')
//index.js
//获取应用实例
var app = getApp()
var sta = require("../../utils/statistics.js");
Page({
  data: {
    indicatorDots: false,//是否显示面板指示点
    autoplay: false,  //是否自动切换
    current:0,      //当前所在index
    interval: 0, //自动切换时间
    duration: 200,  //滑动时间
    catId:"0",
    goodsList:[],
    searchPageNum:0,
    callbackcount:5,
    orderBy: "",
    extdata: {}
  },
  goUrl:function(event){
    var url = event ? event.currentTarget.dataset.url : '';
    if(url.indexOf('/pages/list/index')>0){
      return false;
    }
    util.goUrl(url,this);
  },
  onLoad:function(options){
    this.setData({
      extdata: options
    });
    if (options.shopid && parseInt(options.shopid) > 0) {
      wx.setStorageSync('shopid', options.shopid); //临时记录
    }
    var keyword=options.keyword?options.keyword:'';
    this.setData({
        searchKeyword:keyword
    });
    let shopInfo = wx.getStorageSync('shopInfo');
    wx.setNavigationBarColor({
      frontColor: '#' + (shopInfo.nav_color && shopInfo.nav_color != '' ? shopInfo.nav_color : '000000').toLowerCase(),
      backgroundColor: '#' + (shopInfo.nav_bgcolor && shopInfo.nav_bgcolor != '' ? shopInfo.nav_bgcolor : 'ffffff').toLowerCase(),
    })
	  this.getGoodsCate();
  },
  onReady () {
      var that = this;
      http.request("/banner/getFooter" ,{},function(res){
      if(res.code == 0){
        that.setData({footerNav:res.data});
      }
      });
  },
  onShow:function(){
        sta();
  },
  getGoodsCate:function(){
    var that = this;
    http.request("/item/getCateList", that.data.extdata,function(res){
      if(res.data.code == 0){
        var list = res.data.data;
        var goodsData = [{id:0,title:"全部"}];
        for(var i=1;i<list.length+1;i++){
          goodsData[i]= {id:list[i-1].id,title:list[i-1].title};
        }
        that.setData({goodsData:goodsData});
        that.fetchSearchList();
      }
    });
  },
  getAllGoodsList: function (param, pageindex, callbackcount, callback) {
    var that = this;
    if (param) {
      var order = param.order;
    }
    http.request("/item/allList", {}, function (res) {
      if (res.data.code == 0) {
        console.log(param.cat_id);
        var list = res.data.data;
        var goodsList = [];
        if (param.cat_id>0){
          for (let i in list) {
            if (param.cat_id == list[i].cat_id) {
              goodsList.push(list[i])
            }
          }
        } else if (param.keyword != '') {
          for (let i in list) {
            var keyword = param.keyword.toLowerCase();
            if (list[i].title.toLowerCase().indexOf(keyword) >= 0 || list[i].keywords.toLowerCase().indexOf(keyword)>=0) {
              goodsList.push(list[i])
            }
          }
        } else {
          goodsList = list;
        }
        that.setData({
          isFromSearch: true,
          searchPageNum: 0
        })
        typeof callback == "function" && callback(goodsList,true)
      }
    });
  },
  getGoodsList: function (param, pageindex, callbackcount, callback){
        var that = this;
        var page = this.data.page;
        var data = {
          page: pageindex, 
          page_size: callbackcount, 
        }
        if(param){
          data.title=param.keyword;
          data.order=param.order;
          data.cat_id=param.cat_id;
        }
        http.request("/item/list" ,data,function(res){
          if(res.data.code == 0){
            var list = res.data.data;
            typeof callback == "function" && callback(list, list.length < callbackcount)
          }
        },-1);
  },
  //输入框事件，每输入一个字符，就会触发一次  
  bindKeywordInput: function (e) {
    this.setData({
      searchKeyword: e.detail.value,
      catId: 0,
      orderBy: ""
    })
  },
  getCate: function (event) {
    let cid = event ? event.currentTarget.dataset.id : "0";
    this.setData({
      isFromSearch: true,
      catId: cid,
      searchPageNum: 0,
      orderBy: "",
      searchKeyword:"",
      searchLoadingComplete: false //把“没有数据”设为false，隐藏  
    })
    this.fetchSearchList();
  },
  orderBy:function(event){
    let orderBy = event ? event.currentTarget.dataset.val : "";
    var norder=this.data.orderBy;
    if (orderBy.indexOf('price')>=0){
      if (norder == 'price_asc') {
        orderBy = 'price_desc';
      } else if (norder = "price_desc") {
        orderBy = 'price_asc';
      }
    }
    
    this.setData({
      isFromSearch: true,
      orderBy: orderBy,
      searchPageNum:0,
      searchLoadingComplete: false //把“没有数据”设为false，隐藏  
    })
    this.fetchSearchList();
  },
  loadMore:function(){
    if (!this.data.searchLoadingComplete){
      var searchPageNum = this.data.searchPageNum;
      this.setData({
        isFromSearch: false,
        searchPageNum: searchPageNum + 1
      })
      this.fetchSearchList();
    }
    
  },
  //点击搜索按钮，触发事件  
  keywordSearch: function (e) {
    this.setData({
      searchPageNum: 0,   //第一次加载，设置1    
      isFromSearch: true,  //第一次加载，设置true  
      searchLoading: true,  //把"上拉加载"的变量设为true，显示  
      searchLoadingComplete: false //把“没有数据”设为false，隐藏  
    })
    this.fetchSearchList();
  },
  //搜索，访问网络  
  fetchSearchList: function () {
    let that = this;
    let searchKeyword = that.data.searchKeyword,//输入框字符串作为参数  
      searchPageNum = that.data.searchPageNum,//把第几次加载次数作为参数  
      callbackcount = that.data.callbackcount, //返回数据的个数  
      orderBy = that.data.orderBy,
      cateId = that.data.catId;
    //访问网络
    that.setData({
      loadingText: "正在加载..." 
    });
    var param = { "keyword": that.data.searchKeyword, "order": orderBy, "cat_id": cateId}
    this.getAllGoodsList(param, searchPageNum, callbackcount, function (data,complete) {
      //判断是否有数据，有则取数据  
      if (data) {
        let searchList = that.data.isFromSearch?data:that.data.goodsList.concat(data);
        that.setData({
          goodsList: searchList, //获取数据数组
          searchLoadingComplete: complete, //把“没有数据”设为true，显示 
          searchLoading: !complete,   //把"上拉加载"的变量设为true，显示  
          loadingText: complete ? "加载完成":"点击加载更多..."
        });
      }
    })
  }
})
